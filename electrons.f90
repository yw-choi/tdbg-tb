module electrons
  use mpi
  use env, only: &
    data_dir

  use kinds, only: &
    DP

  use lattice, only: &
    latt, relatt, &
    na_uc, atoms_uc, &
    na_sc, atoms_sc, &
    sc_uc_map, neighbors, &
    num_neighbors, max_neighbors, &
    isc_map, R_isc 
                      
  use constants, only: &
    RYTOEV, PI, TPI, ANGSTROM_AU

  use kgrid, only: &
    kpoint_grid, &
    read_kpoints_band, &
    nk, dimk, xk, wk, &
    nkb, xkb

  use io, only: &
    io_2d_real_fmt

  use electrons_solver_lapack, only: &
    solve_tb_lapack

  use electrons_solver_elpa, only: &
    solve_tb_elpa

  use electrons_solver_feast, only: &
    solve_tb_feast, feast_M0

  use ktetra, only: &
    tetra, tetra_type, tetra_init, tetra_dos_t, &
    ntetra, tetra_weights

  implicit none
  public

  real(dp) :: &
    e_f0, &
    N_F0   ! DOS per spin at charge neutral point (enk = 0)

  integer :: &
    nbnd, &         ! number of bands
    lowest_band, &   ! first band index
    highest_band     ! last band index

  logical :: &
    write_cnk, &
    read_enk, &
    calc_dos, &
    uniform_n_grid

  character(len=100) :: solver ! 'lapack', 'feast', 'elpa'

  logical :: &
    calc_band, &
    write_cnk_band

  real(dp), allocatable :: &
    enk(:,:), &      ! enk(nbnd,nk) electron eigenvalues (Ry).
    enkb(:,:)

contains

  subroutine setup_band_indices
    call printblock_start ('Band indices')

    !! band indices
    if (lowest_band.ge.1.and.highest_band.ge.1 & 
        .and.lowest_band.gt.highest_band) then
      call errore('setup_band_indices', 'lb should be greater than hb', 1)
    endif

    if (lowest_band.le.0) lowest_band = 1
    if (highest_band.le.0) highest_band = na_uc

    if (solver.eq.'feast') then
      nbnd = feast_M0
    else
      nbnd = highest_band-lowest_band+1
    endif

    if (master) then
      print *, 'na_uc / 2 = ', na_uc / 2
      print *, "lowest band = ", lowest_band
      print *, "highest band = ", highest_band
      print *, "nbnd = ", nbnd
    endif

    call printblock_end
  end subroutine setup_band_indices

  subroutine load_enk
    character(len=100) :: fn

    if (.not.allocated(enk)) allocate(enk(nbnd,nk)) 

    write(fn, '(a)') trim(data_dir)//'/enk.dat'
    call io_2d_real_fmt ('read', fn, nbnd, nk, enk, e_f0)
  end subroutine load_enk

  subroutine solve_tb
    integer :: ia
    character(len=100) :: fn

    call printblock_start ('Solving TB in the irreducible BZ')
    call start_clock('solve_tb')

    if (.not.allocated(enk)) allocate(enk(nbnd,nk)) 

    if (solver.eq.'lapack') then
      call solve_tb_lapack (nk, xk, nbnd, &
        lowest_band, highest_band, write_cnk, .false., enk)
    else if (solver.eq.'elpa') then
      call solve_tb_elpa (nk, xk, nbnd, lowest_band, highest_band, &
                          write_cnk, .false., enk)
    else if (solver.eq.'feast') then
      call solve_tb_feast (nk, xk, write_cnk, .false., enk)
    else
      call errore('electrons', 'unavailable solver', 1)
    endif

    if (solver.eq.'lapack'.or.solver.eq.'elpa') then
      call timestamp('Fermi energy start')
      call find_fermi_energy 
      if (master) then
        print *, "e_f0 (eV) = ", e_f0 * RYTOEV
      endif
      call timestamp('Fermi energy end')

      enk = enk - e_f0
    else
      ! Using FEAST solver, it is not possible to calculate Fermi energy
      e_f0 = 0.d0
    endif

    if (master) then
      print *, "Writing eigenvalues..."
      write(fn, '(a)') trim(data_dir)//'/enk.dat'
      call io_2d_real_fmt ('write', fn, nbnd, nk, enk, e_f0)
    endif

    call printblock_end
    call stop_clock ('solve_tb')

  end subroutine solve_tb

  subroutine solve_tb_band

    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), ik_offset, nk_loc
    integer :: ik_loc, ik, ib
    complex(dp), allocatable :: Hmat(:,:), un(:,:)
    real(dp) :: k_cart(3)

    character(len=100) :: fn

    call printblock_start ('Solving TB along the band lines')
    call start_clock ('solve_tb_band')

    if (.not.allocated(enkb)) allocate(enkb(nbnd,nkb)) 

    if (solver.eq.'lapack') then
      call solve_tb_lapack (nkb, xkb, nbnd, &
        lowest_band, highest_band, write_cnk_band, .true., enkb)
    else if (solver.eq.'elpa') then
      call solve_tb_elpa (nkb, xkb, nbnd, lowest_band, highest_band, &
                          write_cnk_band, .true., enkb)
    else if (solver.eq.'feast') then
      call solve_tb_feast (nkb, xkb, write_cnk_band, .true., enkb)
    else
      call errore('electrons', 'unavailable solver', 1)
    endif

    if (solver.eq.'lapack'.or.solver.eq.'elpa') then
      enkb = enkb - e_f0
    endif

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/enk_band.dat'
      call io_2d_real_fmt ('write', fn, nbnd, nkb, enkb, e_f0)
    endif

    call mpi_barrier(comm, mpierr)

    call printblock_end
    call stop_clock ('solve_tb_band')
  end subroutine solve_tb_band

  subroutine find_fermi_energy
    real(dp) :: nelec

    !! Finding the Fermi energy
    e_f0 = 0.d0
    nelec = dble(na_uc) - (lowest_band-1)*2
    call fermi_energy ( nelec, nbnd, nk, enk, e_f0 )

  end subroutine find_fermi_energy

  subroutine fermi_energy ( nelec, nbnd, nkstot, et, ef )
    integer, intent(in) :: nbnd, nkstot
    real(dp), intent(in) :: nelec, et(nbnd,nkstot)
    real(dp), intent(out) :: ef

    integer :: nspin, isk(nkstot), is

    real(dp) :: wg(nbnd, nkstot), dwg(nbnd, nkstot)

    real(dp), external :: efermit

    isk = 1
    is = 0
    nspin = 1

    call start_clock('fermi_e')

    !
    ! ... calculate weights for the metallic case using tetrahedra
    !
    CALL tetra_weights( nkstot, nspin, nbnd, nelec, &
                       ntetra, tetra, et, ef, wg, dwg, 0, isk )

    call stop_clock('fermi_e')
  end subroutine fermi_energy

end module electrons
