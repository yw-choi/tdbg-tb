module mpi
  public
  include 'mpif.h'
  integer :: &
      nprocs, & ! number of processors
      rank, & ! rank of the current processor
      mpierr, & ! mpi error code
      comm      ! MPI_Comm_world
  
  logical :: master
contains

  subroutine mpi_initialize ()
    comm = MPI_Comm_world

    call MPI_Init ( mpierr )
    call MPI_Comm_size ( comm, nprocs, mpierr )
    call MPI_Comm_rank ( comm, rank, mpierr )

    master = rank.eq.0
  end subroutine mpi_initialize

end module mpi
