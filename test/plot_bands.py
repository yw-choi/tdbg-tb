from numpy import *
import matplotlib.pyplot as plt
import glob

enk = 13.605*loadtxt('data/enk_band.dat')
nk, nb = shape(enk)
for ib in range(nb):
    plt.plot(enk[:,ib],'k-')
enk = 13.605*loadtxt('../../../03.M6N5/data/enk_band.dat')
nk, nb = shape(enk)
for ib in range(nb):
    plt.plot(enk[:,ib],'r-')
plt.ylim(-1,1)
plt.ylabel("Energy (eV)")
plt.xticks([0,100,200,300], ['$K$', '$\Gamma$', '$M$', '$K$'])
plt.show()
