from numpy import *
import matplotlib.pyplot as plt
data = loadtxt('data/edos.dat')
plt.figure(0)
plt.plot(data[:,0], data[:,1], '-')
plt.title('TDOS')

data = loadtxt('./data/layer_pdos.dat')

for il in range(4):
    plt.figure(1+il)
    plt.plot(data[:,0], data[:,1+il], '-')
    plt.title('Layer %d' % il)
plt.show()
