module dos

  use mpi
  use env, only: &
    data_dir
  use kinds, only: &
    dp

  use lattice, only: &
    latt, relatt, nlayer, na_uc, na_uc_l
    
  use constants, only: &
    RYTOEV, PI, TPI, ANGSTROM_AU

  use io, only: &
    io_2d_real_fmt, &
    io_2d_cmplx

  use ktetra, only: &
    tetra, tetra_type, tetra_init, tetra_dos_t, &
    ntetra, tetra_weights, linear_tetra_weights_only

  use electrons, only: &
    nbnd, enk, lowest_band, highest_band

  use kgrid, only: &
    nk, nkb

  implicit none

  public :: &
    dos_emin, dos_emax, dos_ne, dos_uniform_n_grid, &
    setup_egrid, &
    calc_tdos, &
    l_layer_pdos, calc_layer_pdos, &
    l_layer_weights_band, calc_layer_weights_band

  private

  logical :: &
    l_layer_pdos, &
    l_layer_weights_band, &
    dos_uniform_n_grid

  integer :: &
    dos_ne

  real(dp) :: &
    dos_emin, &
    dos_emax

  real(dp), allocatable :: &
    tdos(:,:) ! tdos(2,dos_ne) energy (Ry) DOS (states/spin/Ry)
contains

  subroutine calc_tdos 
    integer :: ie, ie_loc
    character(len=100) :: fn
    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), ie_offset, ne_loc

    call start_clock('calc_tdos')
    call printblock_start ('Computing Density of States')

    !! Distribution over energy
    call distribute_indices(dos_ne, nprocs, rank, displs, recvcounts)
    ie_offset = displs(rank)
    ne_loc = recvcounts(rank)

    if (master) then
      print *, "Density of states calculation"
    endif

    do ie_loc = 1, ne_loc
      ie = ie_loc + ie_offset
      call dos_at_e( tdos(1,ie), tdos(2,ie) )
    enddo

    call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                  tdos, 2*recvcounts, 2*displs, &
                  mpi_double_precision,comm,mpierr)

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/edos.dat'
      print '(a,a)', "Saving electron DOS to ", trim(fn)

      open(11, file=trim(fn), form="formatted", status="unknown")
      write(11,'(a,2F10.6, I)') "# emin (eV), emax (eV), ne = ", &
                  dos_emin*RYTOEV, &
                  dos_emax*RYTOEV, &
                  dos_ne 

      write(11,*) "# energy (eV)     DOS (states/spin/eV)"
      do ie = 1, dos_ne
        write(11, *) tdos(1,ie)*RYTOEV, tdos(2,ie)/RYTOEV
      enddo
      close(11)
    endif
    call mpi_barrier(comm, mpierr)

    call stop_clock('calc_tdos')
    call printblock_end
  end subroutine calc_tdos

  ! DOS per spin at energy
  subroutine dos_at_e ( energy, dos )
    real(dp), intent(in) :: energy
    real(dp), intent(out) :: dos

    real(dp) :: dosval(2)
    integer :: nspin
    nspin = 1

    call tetra_dos_t( enk, nspin, nbnd, nk, energy, dosval)
    dos = dosval(1)*0.5d0 ! DOS at e_f0 per spin
  end subroutine dos_at_e

  subroutine calc_layer_pdos
    character(len=100) :: fn
    real(dp), allocatable :: wnk(:,:,:), pdos(:,:), egrid(:)
    integer :: ie, ie_loc
    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), &
               ie_offset, ne_loc
    real(dp) :: dummy, wg(nbnd, nk), dwg(nbnd, nk)
    integer :: is, isk(nk), nspin, il
    is = 0
    isk = 1
    nspin = 1

    call start_clock('layer_pdos')
    call printblock_start ('Computing Partial Density of States')

    !! Calculates layer weights
    allocate(wnk(nlayer,nbnd,nk), pdos(1+nlayer,dos_ne))
    allocate(egrid(dos_ne))
    call calc_layer_weights(wnk, nlayer, nbnd, nk, .false.)

    !! Parallelization over DOS energies
    call distribute_indices(dos_ne, nprocs, rank, &
      displs, recvcounts)
    ie_offset = displs(rank)
    ne_loc = recvcounts(rank)

    !! Energy Grid
    do ie = 1, dos_ne
      egrid(ie) = dos_emin + (ie-1)*(dos_emax-dos_emin)/(dos_ne-1)
    enddo

    !! PDOS calculation
    do ie_loc = 1, ne_loc
      ie = ie_loc + ie_offset

      !! Get tetrahedron weights
      call linear_tetra_weights_only(nk, nspin, is, isk, nbnd, ntetra, tetra, &
        enk, egrid(ie), wg, dwg)

      !! Calculate PDOS
      do il = 1, nlayer
        pdos(1+il, ie) = sum(wnk(il,:,:)*dwg(:,:))
      enddo
    enddo
  
    call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                  pdos, (1+nlayer)*recvcounts, (1+nlayer)*displs, &
                  mpi_double_precision,comm,mpierr)

    pdos(1,:) = egrid*RYTOEV  
    pdos(2:1+nlayer, :) = pdos(2:1+nlayer, :)/RYTOEV/2.d0
    if (master) then
      dummy = 0.d0
      write(fn, '(a)') trim(data_dir)//'/layer_pdos.dat'
      call io_2d_real_fmt('write', fn, 1+nlayer, dos_ne, pdos, dummy)
    endif

    call stop_clock('layer_pdos')
    call printblock_end

  end subroutine calc_layer_pdos

  subroutine calc_layer_weights(wnk, nl, nb, nks, band)
    logical, intent(in) :: band
    integer, intent(in) :: nl, nb, nks
    real(dp), intent(out) :: wnk(nl,nb,nks)

    complex(dp), allocatable :: cnk(:,:)

    integer :: ik, ibnd, il, i1, i2

    character(len=100) :: fn


    if (master) then
      allocate(cnk(na_uc, nb))

      wnk = 0.d0
      do ik = 1, nks

        if (band) then
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk_band.', ik, '.dat'
        else
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk.', ik, '.dat'
        endif

        call io_2d_cmplx ( 'read', fn, na_uc, nb, cnk )

        do ibnd = 1, nb
          do il = 1, nl
            i1 = (il-1)*na_uc_l + 1
            i2 = il*na_uc_l
            wnk(il, ibnd, ik) = abs(sum(conjg(cnk(i1:i2,ibnd))*cnk(i1:i2,ibnd)))
          enddo
        enddo
      enddo
    endif

    call mpi_bcast(wnk, nl*nb*nks, mpi_double_precision, &
      0, comm, mpierr)
  end subroutine calc_layer_weights

  subroutine calc_layer_weights_band
    real(dp), allocatable :: wnkb(:,:,:)
    real(dp) :: dummy, wsum
    character(len=100) :: fn
    integer :: il, ik, ib
    call start_clock('wnkb')
    call printblock_start ('Computing Layer weights for band structure')

    allocate(wnkb(nlayer,nbnd,nkb))
    call calc_layer_weights(wnkb, nlayer, nbnd, nkb, .true.)

    ! normalize
    do ik = 1, nkb
      do ib = 1, nbnd
        wsum = sum(wnkb(:,ib,ik))
        wnkb(:,ib,ik) = wnkb(:,ib,ik)/wsum
      enddo
    enddo
    

    dummy = 0.d0
    do il = 1, nlayer
      write(fn, '(a,I0.6,a)') trim(data_dir) &
        // '/wnk_band.', il, '.dat'
      call io_2d_real_fmt('write', fn, nbnd, nkb, wnkb(il,:,:), dummy)
    enddo

    call stop_clock('wnkb')
    call printblock_end
  end subroutine calc_layer_weights_band

  subroutine setup_egrid
    integer :: i, nmax
    real(dp) :: energy, wg(nbnd,nk), dwg(nbnd,nk), n, ngrid(dos_ne)
    integer :: is, isk(nk)

    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), &
      i_offset, ni_loc, i_loc
    is = 0
    isk = 1

    allocate(tdos(2,dos_ne))
    if (dos_uniform_n_grid) then
      if (master) then
        print *, 'Finding uniform n grid'
      endif
      nmax = (highest_band-lowest_band+1)*2.d0
      call distribute_indices(dos_ne, nprocs, rank, displs, recvcounts)
      i_offset = displs(rank)
      ni_loc = recvcounts(rank)
      do i_loc = 1, ni_loc
        i = i_offset + i_loc
        n = dble(i-1)*nmax/(dos_ne-1)

        call tetra_weights(nk,1,nbnd,n,ntetra,tetra,enk,energy,wg,dwg,is,isk)

        tdos(1,i) = energy
        ngrid(i) = n
      enddo
      call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                    tdos(1,:), recvcounts, displs, &
                    mpi_double_precision,comm,mpierr)
      call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                    ngrid, recvcounts, displs, &
                    mpi_double_precision,comm,mpierr)
      if (master) then
        print *, 'carrier vs. energy (eV) grid'
        do i = 1, dos_ne
          print '(2F20.8)', ngrid(i), tdos(1,i)*RYTOEV
        enddo
      endif
    else
      do i = 1, dos_ne
        tdos(1,i) = dos_emin + (i-1)*(dos_emax-dos_emin)/(dos_ne-1)
      enddo
    endif

  end subroutine setup_egrid
end module dos
