
FC=mpiifort
ELPA_HOME=/lily/ywchoe/opt/elpa
FEAST_HOME=/lily/ywchoe/opt/feast/3.0
IFLAGS=-I./utils -I. \
			 -I${ELPA_HOME}/include/elpa-2018.05.001.rc1/elpa \
			 -I${ELPA_HOME}/include/elpa-2018.05.001.rc1/modules \
			 -I${FEAST_HOME}/include
FCFLAGS=-traceback -O3 -qopenmp
LDFLAGS=

UTIL=utils/libutil.a
LIBS= -mkl -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64  $(UTIL) \
	  ${ELPA_HOME}/lib/libelpa.a \
		${FEAST_HOME}/lib/intel64/libpfeast.a \
		${FEAST_HOME}/lib/intel64/libpfeast_sparse.a

PROGRAMS=tb.x
OBJECTS=mpi.o kind.o timestamp.o constants.o env.o stop_tb.o invmat.o \
				printutils.o sort.o distribute_indices.o kgrid.o \
				cryst_to_car.o erf.o wgauss.o w0gauss.o sumkt.o sumkg.o efermig.o efermit.o tetra.o \
				pzlawrite.o pzeladd.o cal_DOS.o diag.o lattice.o electrons_hoppings.o electrons_solver_lapack.o tblg.o \
				electrons_solver_feast.o electrons_solver_elpa.o io.o electrons.o input.o \
				dos.o berry.o

all: $(PROGRAMS)

input.o: dos.o berry.o electrons.o
dos.o: lattice.o
berry.o: lattice.o mpi.o env.o kind.o constants.o io.o electrons.o kgrid.o

tb.o: $(OBJECTS)
tb.x: tb.o $(UTIL) $(OBJECTS)
	$(FC) -o tb.x tb.o $(IFLAGS) $(FCFLAGS) $(LDFLAGS) $(OBJECTS) $(LIBS) 

$(UTIL):
	cd utils; make
# ======================================================================
# And now the general rules, these should not require modification
# ======================================================================

# General rule for building prog from prog.o; $^ (GNU extension) is
# used in order to list additional object files on which the
# executable depends
%: %.o
	$(FC) $(IFLAGS) $(FCFLAGS) $(LIBS) -o $@ $^ $(LDFLAGS)

# General rules for building prog.o from prog.f90 or prog.F90; $< is
# used in order to list only the first prerequisite (the source file)
# and not the additional prerequisites such as module or include files
%.o: %.f90
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.F90
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.f
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.F
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<

# Utility targets
.PHONY: clean veryclean

clean:
	find . -type f | xargs touch
	rm -f *.o *.mod *.MOD
	cd utils; make clean

veryclean: clean
	rm -f *~ $(PROGRAMS)

electrons.o: electrons_solver_lapack.o electrons_solver_elpa.o io.o electrons_hoppings.o electrons_solver_feast.o
lattice.o: tblg.o
electrons_solver_lapack.o: mpi.o lattice.o io.o
electrons_solver_feast.o: mpi.o lattice.o io.o
electrons_solver_elpa.o: pzlawrite.o lattice.o io.o pzeladd.o
kgrid.o: mpi.o lattice.o env.o tetra.o
