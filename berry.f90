module berry

  use mpi
  use env, only: &
    data_dir
  use kinds, only: &
    dp

  use lattice, only: &
    latt, relatt, nlayer, na_uc, na_uc_l, &
    atoms_uc
    
  use constants, only: &
    RYTOEV, PI, TPI, ANGSTROM_AU

  use io, only: &
    io_2d_real_fmt, &
    io_2d_cmplx

  use electrons, only: &
    nbnd, enk, enkb, lowest_band

  use dos, only: &
    dos_emin, dos_emax, dos_ne

  use kgrid, only: &
    nk, nkb, xkb, kpoints_band_fn

  implicit none
  private

  public :: &
    lberry, &
    calc_berry_phase

  logical :: &
    lberry

contains

  subroutine calc_berry_phase

    integer :: ik, ibnd, jbnd, ia_uc, nbnd_berry
    character(len=100) :: fn
    complex(dp), allocatable :: &
      cnk1(:,:), cnk2(:,:), &
      Mmat_c(:,:), Mmat_v(:,:)
    real(dp) :: &
      k(3), arg, phi_c, phi_v
    complex(dp) :: &
      cfac, det
    integer :: vb1, vb2, cb1, cb2

    integer, allocatable :: bndidx(:,:)
    real(dp) :: overlap, max_overlap, dummy
    integer :: max_jbnd

    call start_clock('berryphase')
    call printblock_start ('Berry Phase calculation')

    ! In case a band calculation is already done,
    ! just read kpath file.

    ! @TODO hard-coded for conduction & valence bands
    vb1 = na_uc/2-1 - lowest_band + 1
    vb2 = na_uc/2   - lowest_band + 1
    cb1 = na_uc/2+1 - lowest_band + 1
    cb2 = na_uc/2+2 - lowest_band + 1

    allocate(cnk1(na_uc,nbnd),cnk2(na_uc,nbnd))
    allocate(Mmat_c(2,2), Mmat_v(2,2))

    ! sort bands according to the overlap 
    allocate(bndidx(4,nkb))
    bndidx = -1

    do ibnd = 1, 4
      bndidx(ibnd,1) = ibnd
    enddo
    
    do ik = 2, nkb
      write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk_band.', ik-1, '.dat'
      call io_2d_cmplx ('read', fn, na_uc, nbnd, cnk1 )
      k = matmul(relatt, xkb(:,ik-1))
      call attach_site_phase(k, cnk1)

      write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk_band.', ik , '.dat'
      call io_2d_cmplx ('read', fn, na_uc, nbnd, cnk2 )
      k = matmul(relatt, xkb(:,ik))
      call attach_site_phase(k, cnk2)

      do ibnd = 1, 4
        max_overlap = -1.d0
        max_jbnd = -1
        do jbnd = 1, 4
          overlap = abs(sum(dconjg(cnk1(:,bndidx(ibnd,ik-1)+vb1-1))*cnk2(:,jbnd+vb1-1)))
          if (overlap.gt.max_overlap) then
            max_jbnd = jbnd
            max_overlap = overlap
          endif
        enddo
        bndidx(ibnd,ik) = max_jbnd
      enddo
    enddo

    if (master) then

      if (.not.allocated(enkb)) allocate(enkb(nbnd,nkb))
      write(fn, '(a)') trim(data_dir)//'/enk_band.dat'
      call io_2d_real_fmt ('read', fn, nbnd, nkb, enkb, dummy)
      open(11, file=trim(data_dir)//"/enk_band_ordered.dat", form="formatted")
      do ik = 1, nkb
        do ibnd = 1, 4
          write(11, '(F12.6F)', advance='no') enkb(bndidx(ibnd,ik)+vb1-1,ik)
        enddo
        write(11, *)
      enddo
      close(11)
    endif
    call mpi_barrier(comm, mpierr)

    phi_c = 0.d0
    phi_v = 0.d0
    do ik = 1, nkb-1
      write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk_band.', ik, '.dat'
      call io_2d_cmplx ('read', fn, na_uc, nbnd, cnk1 )
      k = matmul(relatt, xkb(:,ik))
      call attach_site_phase(k, cnk1)

      write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk_band.', ik+1 , '.dat'
      call io_2d_cmplx ('read', fn, na_uc, nbnd, cnk2 )
      k = matmul(relatt, xkb(:,ik+1))
      call attach_site_phase(k, cnk2)
      
      Mmat_v(1,1) = sum(dconjg(cnk1(:,vb1))*cnk2(:,vb1))
      Mmat_v(1,2) = sum(dconjg(cnk1(:,vb1))*cnk2(:,vb2))
      Mmat_v(2,1) = sum(dconjg(cnk1(:,vb2))*cnk2(:,vb1))
      Mmat_v(2,2) = sum(dconjg(cnk1(:,vb2))*cnk2(:,vb2))

      det = Mmat_v(1,1)*Mmat_v(2,2)-Mmat_v(2,1)*Mmat_v(1,2)
      ! phi_v = phi_v - atan2(dimag(det), dble(det))
      phi_v = phi_v - imag(log(det))

      Mmat_c(1,1) = sum(dconjg(cnk1(:,cb1))*cnk2(:,cb1))
      Mmat_c(1,2) = sum(dconjg(cnk1(:,cb1))*cnk2(:,cb2))
      Mmat_c(2,1) = sum(dconjg(cnk1(:,cb2))*cnk2(:,cb1))
      Mmat_c(2,2) = sum(dconjg(cnk1(:,cb2))*cnk2(:,cb2))

      det = Mmat_c(1,1)*Mmat_c(2,2)-Mmat_c(2,1)*Mmat_c(1,2)
      phi_c = phi_c - imag(log(det))
      ! phi_c = phi_c - atan2(dimag(det), dble(det))

      if (master) then
        write(12, *) ik, phi_c, phi_v
      endif
    enddo

    call stop_clock('berryphase')
    call printblock_end

  end subroutine calc_berry_phase

  subroutine attach_site_phase (k, cnk)
    real(dp), intent(in) :: k(3)
    complex(dp), intent(inout) :: cnk(na_uc,nbnd)

    integer :: ia_uc
    real(dp) :: arg
    complex(dp) :: cfac

    do ia_uc = 1, na_uc
      arg = -1.0d0*sum(k(1:3)*atoms_uc(1:3,ia_uc))
      cfac = cmplx(cos(arg),sin(arg))
      cnk(ia_uc,:) = cnk(ia_uc,:)*cfac
    enddo
  end subroutine attach_site_phase

end module berry
