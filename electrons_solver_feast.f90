module electrons_solver_feast
  use mpi
  use constants, only: TPI
  use kinds, only: DP

  use env, only: data_dir

  use lattice, only: &
    latt, relatt, &
    na_uc, atoms_uc, &
    na_sc, atoms_sc, &
    sc_uc_map, neighbors, &
    num_neighbors, max_neighbors, &
    isc_map, R_isc

  use electrons_hoppings, only: &
    nnz, csr_ia, csr_ja, &
    nhop, hop_pairs, hop

  use io, only: &
    io_2d_cmplx, &
    io_2d_real_fmt

  use diag, only: &
    diag_feast

  implicit none
  public

  integer :: feast_npool, feast_M0
  real(dp) :: feast_emin, feast_emax

contains

  subroutine solve_tb_feast (nk, kpoints, write_cnk, band, enk)

    integer, intent(in) :: nk
    real(dp), intent(in) :: kpoints(3,nk)
    logical, intent(in) :: write_cnk, band
    real(dp), intent(inout) :: enk(feast_M0, nk)

    integer :: mypool, ipool, pool_rank, pool_size, pool_comm
    integer, allocatable :: nk_pool(:), ik0_pool(:)

    complex(dp), allocatable :: hmat(:), cnk(:,:)

    integer :: ik, ik_loc, ibnd, mpi_stat(MPI_STATUS_SIZE)
    real(dp) :: kvec(3), norm
    character(len=100) :: timestr, fn
    integer, allocatable :: feast_M(:), feast_M_buffer(:)
    complex(dp), external :: zdotc
    real(dp), allocatable :: enk_buffer(:,:)

    if (feast_npool.gt.nk) then
      call errore('solver_feast', &
        'npool cannot be larger than nk', feast_npool)
    endif

    if (mod(nprocs, feast_npool).ne.0) then
      call errore('solver_feast', &
        'nprocs should be divisible by npool', feast_npool)
    endif

    ! divide k-point pools
    allocate(nk_pool(feast_npool),ik0_pool(feast_npool))
    nk_pool(:) = int(nk/feast_npool)
    do ipool = 1, feast_npool
      if (ipool.le.mod(nk,feast_npool)) then
        nk_pool(ipool) = nk_pool(ipool) + 1
      endif
    enddo

    ik0_pool(1) = 0
    do ipool = 2, feast_npool
      ik0_pool(ipool) = sum(nk_pool(1:ipool-1))
    enddo

    pool_size = nprocs/feast_npool

    ! find my pool
    mypool = int(rank/pool_size)+1

    ! split mpi communicator
    call MPI_Comm_split(comm, mypool, rank, pool_comm, mpierr)
    call MPI_Comm_rank(pool_comm, pool_rank, mpierr)
    call MPI_Comm_size(pool_comm, pool_size, mpierr)

    allocate(hmat(nnz),cnk(na_uc,feast_M0))
    allocate(feast_M(nk),enk_buffer(feast_M0,nk))
    allocate(feast_M_buffer(nk))

    do ik_loc = 1, nk_pool(mypool)
      WRITE(timestr, "(a,i5,a,i5)") &
        "Computing kpt # ", ik_loc, '/', nk_pool(mypool)
      call timestamp (timestr)

      ik = ik0_pool(mypool)+ik_loc
      kvec = kpoints(1:3, ik)

      call build_hmat_csr(kvec, hmat)

      call diag_feast(pool_comm, na_uc, nnz, hmat, &
        csr_ia, csr_ja, feast_emin, feast_emax, feast_M0, &
        enk(:,ik), cnk, feast_M(ik))

      if (write_cnk) then
        if (pool_rank.eq.0) then
          if (band) then
            write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk_band.', ik, '.dat'
          else
            write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk.', ik, '.dat'
          endif

          call start_clock('io_cnk')
          call io_2d_cmplx ( 'write', fn, na_uc, feast_M0, cnk )
          call stop_clock ('io_cnk')
        endif

        call mpi_barrier(pool_comm, mpierr)
      endif
    enddo

    call MPI_Comm_free(pool_comm, mpierr)

    ! Gather eigenvalues
    if (mypool.ne.1.and.pool_rank.eq.0) then

      call MPI_Send(enk(:,ik0_pool(mypool)+1:ik0_pool(mypool)+nk_pool(mypool)), &
        feast_M0*nk_pool(mypool), mpi_double_precision, &
        0, mypool, comm, mpierr)

      call MPI_Send(feast_M(ik0_pool(mypool)+1:ik0_pool(mypool)+nk_pool(mypool)), &
        nk_pool(mypool), mpi_integer, 0, mypool, comm, mpierr)
    endif

    if (master) then
      do ipool = 2, feast_npool
        call MPI_Recv(enk_buffer(:,1:nk_pool(ipool)), &
          feast_M0*nk_pool(ipool), mpi_double_precision, &
          pool_size*(ipool-1), ipool, comm, mpi_stat, mpierr)

        enk(:,ik0_pool(ipool)+1:ik0_pool(ipool)+nk_pool(ipool)) = enk_buffer(:,1:nk_pool(ipool))

        call MPI_Recv(feast_M_buffer(1:nk_pool(ipool)), &
          nk_pool(ipool), mpi_integer, &
          pool_size*(ipool-1), ipool, comm, mpi_stat, mpierr)
        
        feast_M(ik0_pool(ipool)+1:ik0_pool(ipool)+nk_pool(ipool)) = feast_M_buffer(1:nk_pool(ipool))
      enddo
    endif

    call mpi_bcast(enk, feast_M0*nk, mpi_double_precision, &
      0, comm, mpierr)
    call mpi_bcast(feast_M, nk, mpi_integer, &
      0, comm, mpierr)

    if (master) then
      if (band) then
        open(11, file=trim(data_dir)//'/feast_M_band.dat', form='formatted')
      else
        open(11, file=trim(data_dir)//'/feast_M.dat', form='formatted')
      endif
      do ik = 1, nk
        write(11, *) feast_M(ik)
      enddo
      close(11)
    endif

  end subroutine solve_tb_feast

  subroutine build_hmat_csr(kvec, hmat)
    real(dp), intent(in) :: kvec(3)
    complex(dp), intent(out) :: hmat(nnz)

    integer :: ihop, ia_uc, ja_sc, ja_uc, k, R(3)
    complex(dp) :: cfac
    real(dp) :: arg

    hmat = 0.d0
    do ihop = 1, nhop
      ia_uc = hop_pairs(1,ihop)
      ja_sc = hop_pairs(2,ihop)
      ja_uc = sc_uc_map(ja_sc)
      R = R_isc(1:3, isc_map(ja_sc))
      arg = TPI*sum(kvec*R)
      cfac = cmplx(cos(arg),sin(arg),kind=dp)

      k = find_index(csr_ia(ia_uc+1)-csr_ia(ia_uc), &
        csr_ja(csr_ia(ia_uc):csr_ia(ia_uc+1)), ja_uc)

      hmat(csr_ia(ia_uc)+k-1) = &
        hmat(csr_ia(ia_uc)+k-1) + cfac*hop(ihop)
    enddo

  end subroutine build_hmat_csr

  integer function find_index(n, ar, val) result(idx)
    integer :: n, ar(n), val

    integer :: i 

    idx = -1
    do i = 1, n
      if (ar(i).eq.val) then
        idx = i
        return
      endif
    enddo
    return
  end function find_index
end module electrons_solver_feast
