module input

  use mpi
  use constants, only: &
    RYTOEV, ANGSTROM_AU, AMU_RY

  use env, only: data_dir

  use lattice, only: &
    read_struct, struct_fn, &
    nlayer, M, N, alat, d_layer, shift, symmetrize

  use electrons, only: &
    solver, lowest_band, highest_band, &
    write_cnk, write_cnk_band, &
    read_enk

  use electrons_hoppings, only: &
    rcut, efield

  use kgrid, only: &
    dimk, kpoints_band_fn

  use dos, only: &
    dos_emin, dos_emax, dos_ne, &
    dos_uniform_n_grid, &
    l_layer_pdos, l_layer_weights_band

  use electrons_solver_feast, only: &
    feast_npool, &
    feast_M0, &
    feast_emin, &
    feast_emax

  use berry, only: &
    lberry

  implicit none
  public

contains

  subroutine read_input

    logical :: exst
    logical, external :: makedirqq

    call printblock_start ( "Input parameters" )

    !! &input
    read_struct = .false.
    data_dir = "./data"

    M = 0
    N = 1
    alat = 2.46d0
    d_layer = 3.35d0
    shift(1:2) = 0.d0
    symmetrize = .true.
    nlayer = -1

    rcut = 5.d0 * ANGSTROM_AU
    efield = 0.d0

    read_enk = .false.
    lowest_band = -1
    highest_band = -1

    solver = 'lapack'
    dimk = 1
    kpoints_band_fn = ''

    write_cnk = .false.
    write_cnk_band = .false.

    dos_emin = -2.d0/RYTOEV
    dos_emax = 2.d0/RYTOEV
    dos_ne   = 100
    dos_uniform_n_grid = .false.

    l_layer_pdos = .false.
    l_layer_weights_band = .false.
    lberry = .false.

    feast_npool = 1
    feast_M0 = -1
    feast_emin = 0.d0
    feast_emax = 0.d0

    namelist/input/ data_dir, read_struct, struct_fn, &
                    M, N, alat, d_layer, shift, &
                    solver, &
                    rcut, lowest_band, highest_band, &
                    dimk, write_cnk, &
                    dos_emin, dos_emax, dos_ne, &
                    write_cnk_band, &
                    kpoints_band_fn, read_enk, efield, &
                    symmetrize, dos_uniform_n_grid, &
                    nlayer, lberry, feast_npool, feast_M0, &
                    feast_emin, feast_emax, &
                    l_layer_pdos, l_layer_weights_band

    if (master) then
      read(5, input)
      write(6, input)

      inquire(directory=trim(data_dir), exist=exst)

      if (.not.exst) then
        if (.not.makedirqq(data_dir)) then
          call errore('read_input', 'Failed to create data directory', 1)
        endif
      endif

      alat = alat * ANGSTROM_AU
      d_layer = d_layer * ANGSTROM_AU
      rcut = rcut * ANGSTROM_AU
      dos_emin = dos_emin / RYTOEV
      dos_emax = dos_emax / RYTOEV
      efield = efield / RYTOEV / ANGSTROM_AU
      feast_emin = feast_emin / RYTOEV
      feast_emax = feast_emax / RYTOEV

      if (mod(nlayer,2).ne.0) then
        call errore('input', 'nlayer should be even', nlayer)
      endif
    endif

    call mpi_bcast(read_struct, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(data_dir, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(solver, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(nlayer, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(M, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(N, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(alat, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(d_layer, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(shift, 2, mpi_double_precision, 0, comm, mpierr)

    call mpi_bcast(rcut, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(lowest_band, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(highest_band, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(dimk, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(write_cnk, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(dos_emin, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(dos_emax, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(dos_ne, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(symmetrize, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(write_cnk_band, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(dos_uniform_n_grid, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(kpoints_band_fn, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(read_enk, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(efield, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(lberry, 1, mpi_logical, 0, comm, mpierr)

    call mpi_bcast(feast_emin, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(feast_emax, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(feast_npool, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(feast_M0, 1, mpi_integer, 0, comm, mpierr)

    call mpi_bcast(l_layer_weights_band, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(l_layer_pdos, 1, mpi_logical, 0, comm, mpierr)

    call printblock_end

  end subroutine read_input

end module input
