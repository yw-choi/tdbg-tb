c written by H. J. Choi      
      subroutine cal_dweight(nband,nk,dg1,dg2,dg3,ei,efermi,
     &                       n1,n2,n3,weight,dweight,anel)
c  
      implicit none
      real*8 dg1(3),dg2(3),dg3(3),ei(nband,nk),
     &       weight(nband,nk),dweight(nband,nk),
     &       dg(3),ei_tet(4),efermi,anel
      integer, allocatable :: itetpoint(:,:)
      integer k_tet(4),nband,nk,n1,n2,n3,ntet,
     &        i,j,k,imin,jmin,kmin,L1,L2,L3,L4,L5,L6,L7,L8,
     &        ip,jp,kp,itet,iband
      real*8 e1,e2,e3,e4,ef1,ef2,ef3,ef4,e21,e31,e41,e32,e42,e43,
     &       w1,w2,w3,w4,dw1,dw2,dw3,dw4,dos,c1,c2,c3,dc1,dc2,dc3,
     &       total,ef,ef_min,ef_max,dglen,dglen_min,xx,c,dc
c
      ntet = nk*6
      allocate(itetpoint(4,ntet))
      dglen_min = 1e10
      do i = -1,1,2
         do j = -1,1,2
            do k = -1,1,2
               dg(1) = i*dg1(1)+j*dg2(1)+k*dg3(1)
               dg(2) = i*dg1(2)+j*dg2(2)+k*dg3(2)
               dg(3) = i*dg1(3)+j*dg2(3)+k*dg3(3)
               dglen = sqrt(dg(1)*dg(1)+dg(2)*dg(2)+dg(3)*dg(3))
               if(dglen.lt.dglen_min) then
                  dglen_min = dglen
                  imin = i
                  jmin = j
                  kmin = k
               endif
            enddo
         enddo
      enddo
      itet = 0
      do i = 0,n1-1
         ip = i+imin
         if(ip.eq.-1)ip=n1-1
         if(ip.eq.n1)ip=0
         do j = 0,n2-1
            jp = j+jmin
            if(jp.eq.-1)jp=n2-1
            if(jp.eq.n2)jp=0
            do k = 0,n3-1
               kp = k+kmin
               if(kp.eq.-1)kp=n3-1
               if(kp.eq.n3)kp=0
               L1 = 1 + i + j * n1 + kp* n1 * n2
               L2 = 1 + i + jp* n1 + kp* n1 * n2
               L3 = 1 + i + j * n1 + k * n1 * n2
               L4 = 1 + i + jp* n1 + k * n1 * n2
               L5 = 1 + ip+ j * n1 + kp* n1 * n2
               L6 = 1 + ip+ jp* n1 + kp* n1 * n2
               L7 = 1 + ip+ j * n1 + k * n1 * n2
               L8 = 1 + ip+ jp* n1 + k * n1 * n2
               itet = itet+1
               itetpoint(1,itet) = L1
               itetpoint(2,itet) = L2
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L2
               itetpoint(2,itet) = L4
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L4
               itetpoint(2,itet) = L8
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L8
               itetpoint(2,itet) = L7
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L7
               itetpoint(2,itet) = L5
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L5
               itetpoint(2,itet) = L1
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
            enddo
         enddo
      enddo
      ef = efermi
      ef_max = 1e10
      ef_min = -1e10
 300  do j = 1,nk
         do i = 1,nband
            weight(i,j) = 0.0
            dweight(i,j) = 0.0
         enddo
      enddo
      total = 0.0
      do itet = 1,ntet
         do k = 1,4
            k_tet(k) = itetpoint(k,itet)
         enddo
         do iband = 1,nband
            do k = 1,4
               ei_tet(k) = ei(iband,k_tet(k))
            enddo
            do i = 1,3
               do j = i+1,4
                  if(ei_tet(i).gt.ei_tet(j)) then
                     xx = ei_tet(j)
                     ei_tet(j) = ei_tet(i)
                     ei_tet(i) = xx
                     k = k_tet(j)
                     k_tet(j) = k_tet(i)
                     k_tet(i) = k
                  endif
               enddo
            enddo
            e1 = ei_tet(1)
            e2 = ei_tet(2)
            e3 = ei_tet(3)
            e4 = ei_tet(4)
            ef1 = ef-e1
            ef2 = ef-e2
            ef3 = ef-e3
            ef4 = ef-e4
            e21 = e2-e1
            e31 = e3-e1
            e41 = e4-e1
            e32 = e3-e2
            e42 = e4-e2
            e43 = e4-e3
            if(ef.le.e1) then
               w1 = 0.0
               w2 = 0.0
               w3 = 0.0
               w4 = 0.0
               dw1 = 0.0
               dw2 = 0.0
               dw3 = 0.0
               dw4 = 0.0
            elseif(ef.le.e2) then
               total = total + ef1*ef1*ef1/(e21*e31*e41)
               dos = dos + 3*ef1*ef1/(e21*e31*e41)
               c = ef1*ef1*ef1/(e21*e31*e41)*0.25D0
               dc = 3*ef1*ef1/(e21*e31*e41)*0.25D0
               w1 = c*(4.0-ef1*(1.0/e21+1.0/e31+1.0/e41))
               w2 = c*(ef1/e21)
               w3 = c*(ef1/e31)
               w4 = c*(ef1/e41)
               dw1 = dc*(4.0-ef1*(1.0/e21+1.0/e31+1.0/e41))
     &             -  c*(1.0/e21+1.0/e31+1.0/e41)
               dw2 = dc*(ef1/e21)+c/e21
               dw3 = dc*(ef1/e31)+c/e31
               dw4 = dc*(ef1/e41)+c/e41
            elseif(ef.le.e3) then
               total = total + (e21**2+3*e21*ef2+3*ef2**2
     &               -(e31+e42)/(e32*e42)*ef2**3)/(e31*e41)
               dos = dos + (3*e21+6*ef2
     &               -(e31+e42)/(e32*e42)*3*ef2**2)/(e31*e41)
               c1 = (ef1*ef1)/(e41*e31)*0.25D0
               c2 = -(ef1*ef2*ef3)/(e41*e32*e31)*0.25D0
               c3 = -(ef2*ef2*ef4)/(e42*e32*e41)*0.25D0
               dc1 = 2*ef1/(e41*e31)*0.25D0
               dc2 = -(ef1*ef2+ef2*ef3+ef1*ef3)/(e41*e32*e31)*0.25D0
               dc3 = -(2*ef2*ef4+ef2*ef2)/(e42*e32*e41)*0.25D0
               w1 = c1-(c1+c2)*ef3/e31-(c1+c2+c3)*ef4/e41
               w2 = c1+c2+c3-(c2+c3)*ef3/e32-c3*ef4/e42
               w3 = (c1+c2)*ef1/e31+(c2+c3)*ef2/e32
               w4 = (c1+c2+c3)*ef1/e41+c3*ef2/e42
               dw1 = dc1-(dc1+dc2)*ef3/e31-(dc1+dc2+dc3)*ef4/e41
     &                  -(c1+c2)/e31-(c1+c2+c3)/e41
               dw2 = dc1+dc2+dc3-(dc2+dc3)*ef3/e32-dc3*ef4/e42
     &                  -(c2+c3)/e32-c3/e42
               dw3 = (dc1+dc2)*ef1/e31+(dc2+dc3)*ef2/e32
     &               + (c1+c2)/e31+(c2+c3)/e32
               dw4 = (dc1+dc2+dc3)*ef1/e41+dc3*ef2/e42
     &               +(c1+c2+c3)/e41+c3/e42
            elseif(ef.le.e4) then
               total = total + 1.0+(ef4*ef4*ef4)/(e41*e42*e43)
               dos = dos + 3*(ef4*ef4)/(e41*e42*e43)
               c = -ef4*ef4*ef4/(e41*e42*e43)*0.25D0
               dc = -3.0*ef4*ef4/(e41*e42*e43)*0.25D0
               w1 = 0.25D0+c*ef4/e41
               w2 = 0.25D0+c*ef4/e42
               w3 = 0.25D0+c*ef4/e43
               w4 = 0.25D0-c*(4.0+ef4*(1.0/e41+1.0/e42+1.0/e43)) 
               dw1 = dc*ef4/e41+c/e41
               dw2 = dc*ef4/e42+c/e42
               dw3 = dc*ef4/e43+c/e43
               dw4 = -dc*(4.0+ef4*(1.0/e41+1.0/e42+1.0/e43)) 
     &               -c*(1.0/e41+1.0/e42+1.0/e43)
            else
               total = total + 1.0
               w1 = 0.25D0
               w2 = 0.25D0
               w3 = 0.25D0
               w4 = 0.25D0
               dw1 = 0.0
               dw2 = 0.0
               dw3 = 0.0
               dw4 = 0.0
            endif
            weight(iband,k_tet(1)) = weight(iband,k_tet(1))+w1
            weight(iband,k_tet(2)) = weight(iband,k_tet(2))+w2
            weight(iband,k_tet(3)) = weight(iband,k_tet(3))+w3
            weight(iband,k_tet(4)) = weight(iband,k_tet(4))+w4
            dweight(iband,k_tet(1)) = dweight(iband,k_tet(1))+dw1
            dweight(iband,k_tet(2)) = dweight(iband,k_tet(2))+dw2
            dweight(iband,k_tet(3)) = dweight(iband,k_tet(3))+dw3
            dweight(iband,k_tet(4)) = dweight(iband,k_tet(4))+dw4
         enddo
      enddo
      total = total/float(ntet)*2.0
      dos = dos/float(ntet)*2.0
      do i = 1,nband
         do j = 1,nk
            weight(i,j) = weight(i,j)/float(ntet)*2.0
            dweight(i,j) = dweight(i,j)/float(ntet)*2.0
         enddo
      enddo
      write(6,*)'# ef = ',ef,' eV   total = ',total
      call flush(6)
      if(ef_max-ef_min.gt.0.00001) then
         if(total.lt.anel) then
            ef_min = ef
            if(ef_max.ne.1e10) then
               ef = (ef+ef_max)*0.5
            else
               ef = ef + 0.1
            endif
         elseif(total.gt.anel) then
            ef_max = ef
            if(ef_min.ne.-1e10) then
               ef = (ef+ef_min)*0.5
            else
               ef = ef - 0.1
            endif
         endif
         goto 300
      endif
      write(6,*)'# ef = ',ef,' eV'
      write(6,*)'#  total = ',total
      write(6,*)'#  dos = ',dos,' /eV/unitcell'
      call flush(6)
      deallocate(itetpoint)
      efermi = ef
      return
      end
c
      subroutine cal_DOS(nband,nk,dg1,dg2,dg3,ei,efermi,
     &                   n1,n2,n3,weight,dweight,nfermi,tdos)
c  
      implicit none
      real*8 dg1(3),dg2(3),dg3(3),ei(nband,nk),
     &       weight(nband,nk),dweight(nband,nk),
     &       dg(3),ei_tet(4),efermi(nfermi),tdos(nfermi)
      integer, allocatable :: itetpoint(:,:)
      integer k_tet(4),nband,nk,n1,n2,n3,ntet,
     &        i,j,k,imin,jmin,kmin,L1,L2,L3,L4,L5,L6,L7,L8,
     &        ip,jp,kp,itet,iband,nfermi,ifermi
      real*8 e1,e2,e3,e4,ef1,ef2,ef3,ef4,e21,e31,e41,e32,e42,e43,
     &       w1,w2,w3,w4,dw1,dw2,dw3,dw4,dos,c1,c2,c3,dc1,dc2,dc3,
     &       total,ef,ef_min,ef_max,dglen,dglen_min,xx,c,dc
c
      ntet = nk*6
      allocate(itetpoint(4,ntet))
      dglen_min = 1e10
      do i = -1,1,2
         do j = -1,1,2
            do k = -1,1,2
               dg(1) = i*dg1(1)+j*dg2(1)+k*dg3(1)
               dg(2) = i*dg1(2)+j*dg2(2)+k*dg3(2)
               dg(3) = i*dg1(3)+j*dg2(3)+k*dg3(3)
               dglen = sqrt(dg(1)*dg(1)+dg(2)*dg(2)+dg(3)*dg(3))
               if(dglen.lt.dglen_min) then
                  dglen_min = dglen
                  imin = i
                  jmin = j
                  kmin = k
               endif
            enddo
         enddo
      enddo
      itet = 0
      do i = 0,n1-1
         ip = i+imin
         if(ip.eq.-1)ip=n1-1
         if(ip.eq.n1)ip=0
         do j = 0,n2-1
            jp = j+jmin
            if(jp.eq.-1)jp=n2-1
            if(jp.eq.n2)jp=0
            do k = 0,n3-1
               kp = k+kmin
               if(kp.eq.-1)kp=n3-1
               if(kp.eq.n3)kp=0
               L1 = 1 + i + j * n1 + kp* n1 * n2
               L2 = 1 + i + jp* n1 + kp* n1 * n2
               L3 = 1 + i + j * n1 + k * n1 * n2
               L4 = 1 + i + jp* n1 + k * n1 * n2
               L5 = 1 + ip+ j * n1 + kp* n1 * n2
               L6 = 1 + ip+ jp* n1 + kp* n1 * n2
               L7 = 1 + ip+ j * n1 + k * n1 * n2
               L8 = 1 + ip+ jp* n1 + k * n1 * n2
               itet = itet+1
               itetpoint(1,itet) = L1
               itetpoint(2,itet) = L2
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L2
               itetpoint(2,itet) = L4
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L4
               itetpoint(2,itet) = L8
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L8
               itetpoint(2,itet) = L7
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L7
               itetpoint(2,itet) = L5
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
               itet = itet+1
               itetpoint(1,itet) = L5
               itetpoint(2,itet) = L1
               itetpoint(3,itet) = L3
               itetpoint(4,itet) = L6
            enddo
         enddo
      enddo
      ! write(6,'(a)')'  E(eV)   total    dos(states/eV/unitcell)'
      do ifermi = 1,nfermi
         ef = efermi(ifermi)  ! eV
         do j = 1,nk
            do i = 1,nband
               weight(i,j) = 0.0
               dweight(i,j) = 0.0
            enddo
         enddo
         total = 0.0
         do itet = 1,ntet
            do k = 1,4
               k_tet(k) = itetpoint(k,itet)
            enddo
            do iband = 1,nband
               do k = 1,4
                  ei_tet(k) = ei(iband,k_tet(k))
               enddo
               do i = 1,3
                  do j = i+1,4
                     if(ei_tet(i).gt.ei_tet(j)) then
                        xx = ei_tet(j)
                        ei_tet(j) = ei_tet(i)
                        ei_tet(i) = xx
                        k = k_tet(j)
                        k_tet(j) = k_tet(i)
                        k_tet(i) = k
                     endif
                  enddo
               enddo
               e1 = ei_tet(1)
               e2 = ei_tet(2)
               e3 = ei_tet(3)
               e4 = ei_tet(4)
               ef1 = ef-e1
               ef2 = ef-e2
               ef3 = ef-e3
               ef4 = ef-e4
               e21 = e2-e1
               e31 = e3-e1
               e41 = e4-e1
               e32 = e3-e2
               e42 = e4-e2
               e43 = e4-e3
               if(ef.le.e1) then
                  w1 = 0.0
                  w2 = 0.0
                  w3 = 0.0
                  w4 = 0.0
                  dw1 = 0.0
                  dw2 = 0.0
                  dw3 = 0.0
                  dw4 = 0.0
               elseif(ef.le.e2) then
                  total = total + ef1*ef1*ef1/(e21*e31*e41)
                  dos = dos + 3*ef1*ef1/(e21*e31*e41)
                  c = ef1*ef1*ef1/(e21*e31*e41)*0.25D0
                  dc = 3*ef1*ef1/(e21*e31*e41)*0.25D0
                  w1 = c*(4.0-ef1*(1.0/e21+1.0/e31+1.0/e41))
                  w2 = c*(ef1/e21)
                  w3 = c*(ef1/e31)
                  w4 = c*(ef1/e41)
                  dw1 = dc*(4.0-ef1*(1.0/e21+1.0/e31+1.0/e41))
     &                -  c*(1.0/e21+1.0/e31+1.0/e41)
                  dw2 = dc*(ef1/e21)+c/e21
                  dw3 = dc*(ef1/e31)+c/e31
                  dw4 = dc*(ef1/e41)+c/e41
               elseif(ef.le.e3) then
                  total = total + (e21**2+3*e21*ef2+3*ef2**2
     &               -(e31+e42)/(e32*e42)*ef2**3)/(e31*e41)
                  dos = dos + (3*e21+6*ef2
     &               -(e31+e42)/(e32*e42)*3*ef2**2)/(e31*e41)
                  c1 = (ef1*ef1)/(e41*e31)*0.25D0
                  c2 = -(ef1*ef2*ef3)/(e41*e32*e31)*0.25D0
                  c3 = -(ef2*ef2*ef4)/(e42*e32*e41)*0.25D0
                  dc1 = 2*ef1/(e41*e31)*0.25D0
                  dc2 = -(ef1*ef2+ef2*ef3+ef1*ef3)/(e41*e32*e31)*0.25D0
                  dc3 = -(2*ef2*ef4+ef2*ef2)/(e42*e32*e41)*0.25D0
                  w1 = c1-(c1+c2)*ef3/e31-(c1+c2+c3)*ef4/e41
                  w2 = c1+c2+c3-(c2+c3)*ef3/e32-c3*ef4/e42
                  w3 = (c1+c2)*ef1/e31+(c2+c3)*ef2/e32
                  w4 = (c1+c2+c3)*ef1/e41+c3*ef2/e42
                  dw1 = dc1-(dc1+dc2)*ef3/e31-(dc1+dc2+dc3)*ef4/e41
     &                  -(c1+c2)/e31-(c1+c2+c3)/e41
                  dw2 = dc1+dc2+dc3-(dc2+dc3)*ef3/e32-dc3*ef4/e42
     &                  -(c2+c3)/e32-c3/e42
                  dw3 = (dc1+dc2)*ef1/e31+(dc2+dc3)*ef2/e32
     &                + (c1+c2)/e31+(c2+c3)/e32
                  dw4 = (dc1+dc2+dc3)*ef1/e41+dc3*ef2/e42
     &                 +(c1+c2+c3)/e41+c3/e42
               elseif(ef.le.e4) then
                  total = total + 1.0+(ef4*ef4*ef4)/(e41*e42*e43)
                  dos = dos + 3*(ef4*ef4)/(e41*e42*e43)
                  c = -ef4*ef4*ef4/(e41*e42*e43)*0.25D0
                  dc = -3.0*ef4*ef4/(e41*e42*e43)*0.25D0
                  w1 = 0.25D0+c*ef4/e41
                  w2 = 0.25D0+c*ef4/e42
                  w3 = 0.25D0+c*ef4/e43
                  w4 = 0.25D0-c*(4.0+ef4*(1.0/e41+1.0/e42+1.0/e43)) 
                  dw1 = dc*ef4/e41+c/e41
                  dw2 = dc*ef4/e42+c/e42
                  dw3 = dc*ef4/e43+c/e43
                  dw4 = -dc*(4.0+ef4*(1.0/e41+1.0/e42+1.0/e43)) 
     &                  -c*(1.0/e41+1.0/e42+1.0/e43)
               else
                  total = total + 1.0
                  w1 = 0.25D0
                  w2 = 0.25D0
                  w3 = 0.25D0
                  w4 = 0.25D0
                  dw1 = 0.0
                  dw2 = 0.0
                  dw3 = 0.0
                  dw4 = 0.0
               endif
               weight(iband,k_tet(1)) = weight(iband,k_tet(1))+w1
               weight(iband,k_tet(2)) = weight(iband,k_tet(2))+w2
               weight(iband,k_tet(3)) = weight(iband,k_tet(3))+w3
               weight(iband,k_tet(4)) = weight(iband,k_tet(4))+w4
               dweight(iband,k_tet(1)) = dweight(iband,k_tet(1))+dw1
               dweight(iband,k_tet(2)) = dweight(iband,k_tet(2))+dw2
               dweight(iband,k_tet(3)) = dweight(iband,k_tet(3))+dw3
               dweight(iband,k_tet(4)) = dweight(iband,k_tet(4))+dw4
            enddo
         enddo
         total = total/float(ntet)*2.0
         dos = dos/float(ntet)*2.0
         
         do i = 1,nband
            do j = 1,nk
               weight(i,j) = weight(i,j)/float(ntet)*2.0
               dweight(i,j) = dweight(i,j)/float(ntet)*2.0
            enddo
         enddo
         tdos(ifermi) = dos
         ! write(6,'(2f10.5,e15.5)')ef,total,dos
         ! call flush(6)
      enddo
      deallocate(itetpoint)
      return
      end
c
