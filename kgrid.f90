module kgrid

  use mpi
  use kinds, only: DP
  use constants, only: TPI, ANGSTROM_AU, RYTOEV
  use lattice, only: &
    latt, relatt, symmetrize, nrot, s, t_rev, time_reversal
  use env, only: data_dir
  use ktetra, only: &
    tetra, tetra_type, tetra_init, tetra_dos_t, &
    ntetra, tetra_weights


  implicit none
  public 

  integer :: &
    dimk, &          ! k-points grid dimension
    nkmax, &         ! nkmax=dimk*dimk
    nk,    &         ! number of k-points       
    nkb              ! number of band k-points

  real(dp), allocatable :: &
             xk(:,:),      &  ! k-points in frac. cooridnate
             wk(:),        &  ! k-points weight
             xkb(:,:)         ! band k-points in frac. coord.

  integer, allocatable :: &
    equiv(:),  &
    equiv_s(:)

  character(len=100) :: kpoints_band_fn
contains

  subroutine setup_kgrid
    integer :: ik, ik1, ik2, ik_equiv
    real(dp) :: xk_cart(3), xkg(3)
    call start_clock ('kgrid')
    call printblock_start ('k-point grid')

    ! if symmetry = .false., nk = dimk*dimk,
    !    symmetry = .true. nk = # of irreducible k-points
    nkmax = dimk*dimk
    allocate(xk(3,nkmax), wk(nkmax))
    allocate(equiv(nkmax), equiv_s(nkmax))
    xk = 0.d0
    wk = 0.d0

    if (master) then
      call kpoint_grid (nrot, time_reversal, .not.symmetrize, &
        s, t_rev, nkmax, 0,0,0,dimk,dimk,1, nk, xk, wk, &
        equiv, equiv_s) 

      print *, "k-point grid dimension = ", dimk, "x", dimk
      print *, "Number of k-points = ", dimk*dimk
      if (symmetrize) then
        print *, "Number of irreducible k-points = ", nk
      endif

      open(11, file=trim(data_dir)//"/kpoints.dat", form="formatted")
      write(11, *) nkmax, nk
      do ik = 1, nk
        xk_cart = matmul(relatt,xk(:,ik))*ANGSTROM_AU
        write(11, "(4F10.6,1x,F12.8)") xk(1:2,ik), xk_cart(1:2), wk(ik)
      enddo
      close(11)

      open(11, file=trim(data_dir)//"/kpoints_equiv.dat", form="formatted")
      write(11, *) nkmax
      do ik = 1, nkmax
        ik1 = int((ik-1)/dimk)+1
        ik2 = mod(ik-1,dimk)+1
        xkg(1) = dble(ik1-1)/dimk
        xkg(2) = dble(ik2-1)/dimk
        xkg(3) = 0.d0
        xk_cart = matmul(relatt,xkg)*ANGSTROM_AU
        write(11, "(2F10.6,I10,I5)") xk_cart(1:2), equiv(ik), equiv_s(ik)
      enddo
      close(11)
    endif
    call mpi_bcast(nk, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(xk, 3*nk, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(wk, nk, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(equiv, nk, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(equiv_s, nk, mpi_integer, 0, comm, mpierr)

    ! read band k-points
    if (master) then
      call read_kpoints_band ( kpoints_band_fn, nkb, xkb )
    endif

    call mpi_bcast(nkb, 1, mpi_integer, 0, comm, mpierr)
    if (.not.master) then
      allocate(xkb(3,nkb))
    endif

    call mpi_bcast(xkb, 3*nkb, mpi_double_precision, 0, comm, mpierr)
  
    call setup_tetra

    call printblock_end
    call stop_clock ('kgrid')
  end subroutine setup_kgrid

  SUBROUTINE kpoint_grid ( nrot, time_reversal, skip_equivalence, s, t_rev, &
                           npk, k1,k2,k3, nk1,nk2,nk3, nks, xk, wk, equiv_irr, equiv_s)
  !-----------------------------------------------------------------------
  !
  !  Automatic generation of a uniform grid of k-points
  !
    USE kinds, ONLY: DP
    IMPLICIT NONE
    !
    INTEGER, INTENT(in):: nrot, npk, k1, k2, k3, nk1, nk2, nk3, &
                          t_rev(48), s(3,3,48)
    LOGICAL, INTENT(in):: time_reversal, skip_equivalence
    ! real(DP), INTENT(in):: bg(3,3)
    !
    INTEGER, INTENT(out) :: nks
    real(DP), INTENT(out):: xk(3,npk)
    real(DP), INTENT(out):: wk(npk)
    INTEGER, INTENT(out):: equiv_irr(npk)
    INTEGER, INTENT(out):: equiv_s(npk)
    ! LOCAL:
    real(DP), PARAMETER :: eps=1.0d-5
    real(DP) :: xkr(3), fact, xx, yy, zz
    INTEGER, ALLOCATABLE:: equiv(:), ik2irr(:)
    real(DP), ALLOCATABLE:: xkg(:,:), wkk(:)
    INTEGER :: nkr, i,j,k, ns, n, nk
    LOGICAL :: in_the_list
    !
    nkr=nk1*nk2*nk3
    ALLOCATE (xkg( 3,nkr),wkk(nkr),equiv(nkr),ik2irr(nkr))
    !
    DO i=1,nk1
       DO j=1,nk2
          DO k=1,nk3
             !  this is nothing but consecutive ordering
             n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
             !  xkg are the components of the complete grid in crystal axis
             xkg(1,n) = dble(i-1)/nk1 + dble(k1)/2/nk1
             xkg(2,n) = dble(j-1)/nk2 + dble(k2)/2/nk2
             xkg(3,n) = dble(k-1)/nk3 + dble(k3)/2/nk3
          ENDDO
       ENDDO
    ENDDO

    !  equiv(nk) =nk : k-point nk is not equivalent to any previous k-point
    !  equiv(nk)!=nk : k-point nk is equivalent to k-point equiv(nk)

    DO nk=1,nkr
       equiv(nk)=nk
    ENDDO

    IF ( skip_equivalence ) THEN
      CALL infomsg('kpoint_grid', 'ATTENTION: skip check of k-points equivalence')
      wkk = 1.d0
    ELSE
      DO nk=1,nkr
      !  check if this k-point has already been found equivalent to another
        IF (equiv(nk) == nk) THEN
          wkk(nk)   = 1.0d0
          !  check if there are equivalent k-point to this in the list
          !  (excepted those previously found to be equivalent to another)
          !  check both k and -k
          DO ns=1,nrot
             DO i=1,3
                xkr(i) = s(i,1,ns) * xkg(1,nk) &
                       + s(i,2,ns) * xkg(2,nk) &
                       + s(i,3,ns) * xkg(3,nk)
                xkr(i) = xkr(i) - nint( xkr(i) )
             ENDDO
             IF(t_rev(ns)==1) xkr = -xkr
             xx = xkr(1)*nk1 - 0.5d0*k1
             yy = xkr(2)*nk2 - 0.5d0*k2
             zz = xkr(3)*nk3 - 0.5d0*k3
             in_the_list = abs(xx-nint(xx))<=eps .and. &
                           abs(yy-nint(yy))<=eps .and. &
                           abs(zz-nint(zz))<=eps
             IF (in_the_list) THEN
                i = mod ( nint ( xkr(1)*nk1 - 0.5d0*k1 + 2*nk1), nk1 ) + 1
                j = mod ( nint ( xkr(2)*nk2 - 0.5d0*k2 + 2*nk2), nk2 ) + 1
                k = mod ( nint ( xkr(3)*nk3 - 0.5d0*k3 + 2*nk3), nk3 ) + 1
                n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
                IF (n>nk .and. equiv(n)==n) THEN
                   equiv(n) = nk
                   equiv_s(n) = ns
                   wkk(nk)=wkk(nk)+1.0d0
                ELSE
                   IF (equiv(n)/=nk .or. n<nk ) CALL errore('kpoint_grid', &
                      'something wrong in the checking algorithm',1)
                ENDIF
             ENDIF
             IF ( time_reversal ) THEN
                xx =-xkr(1)*nk1 - 0.5d0*k1
                yy =-xkr(2)*nk2 - 0.5d0*k2
                zz =-xkr(3)*nk3 - 0.5d0*k3
                in_the_list=abs(xx-nint(xx))<=eps.and.abs(yy-nint(yy))<=eps &
                                                   .and. abs(zz-nint(zz))<=eps
                IF (in_the_list) THEN
                   i = mod ( nint (-xkr(1)*nk1 - 0.5d0 * k1 + 2*nk1), nk1 ) + 1
                   j = mod ( nint (-xkr(2)*nk2 - 0.5d0 * k2 + 2*nk2), nk2 ) + 1
                   k = mod ( nint (-xkr(3)*nk3 - 0.5d0 * k3 + 2*nk3), nk3 ) + 1
                   n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
                   IF (n>nk .and. equiv(n)==n) THEN
                      equiv(n) = nk
                      equiv_s(n) = -ns
                      wkk(nk)=wkk(nk)+1.0d0
                   ELSE
                      IF (equiv(n)/=nk.or.n<nk) CALL errore('kpoint_grid', &
                      'something wrong in the checking algorithm',2)
                   ENDIF
                ENDIF
             ENDIF
          ENDDO
        ENDIF
      ENDDO
    ENDIF

    !  count irreducible points and order them
    nks=0
    fact=0.0d0
    DO nk=1,nkr
       IF (equiv(nk)==nk) THEN
          nks=nks+1
          ik2irr(nk) = nks
          IF (nks>npk) CALL errore('kpoint_grid','too many k-points',1)
          wk(nks) = wkk(nk)
          fact    = fact+wk(nks)
          !  bring back into to the first BZ
          DO i=1,3
             xk(i,nks) = xkg(i,nk) - floor(xkg(i,nk))
          ENDDO

       ENDIF
    ENDDO

    DO nk=1,nkr
      equiv_irr(nk) = ik2irr(equiv(nk))
    ENDDO

    !  go to cartesian axis (in units 2pi/a0)
    ! CALL cryst_to_cart(nks,xk,bg,1)
    !  normalize weights to one
    DO nk=1,nks
       wk(nk) = wk(nk)/fact
    ENDDO

    DEALLOCATE(xkg,wkk)

    RETURN
  END SUBROUTINE kpoint_grid

  subroutine setup_tetra
    real(dp), allocatable :: xk_cart(:,:)
    real(dp) :: dos_t(2)
    integer :: i, k1, k2, k3, kstep

    call start_clock('setup_tetra')

    k1 = 0 ! no shifted grid
    k2 = 0
    k3 = 0
    kstep = 1

    allocate(xk_cart(3,nk))
    do i = 1, nk
      xk_cart(:,i) = matmul(relatt, xk(:,i))/TPI
    enddo

    if (master) then
      print *, "Initializing tetrahedrons"
    endif

    tetra_type = 0

    call tetra_init (nrot, s, time_reversal, t_rev, latt, &
      relatt/TPI, nkmax, k1, k2, k3, dimk, dimk, 1, nk, xk)

    call stop_clock('setup_tetra')

  end subroutine setup_tetra

  subroutine read_kpoints_band ( fn, nk_band, kpoints_band )
    character(len=100) :: fn
    integer, intent(out) :: nk_band
    real(dp), allocatable, intent(out) :: kpoints_band(:,:)

    logical :: exst
    integer :: ik 

    inquire(file=trim(fn), exist=exst)
    if (.not.exst) then
      nk_band = 0
      allocate(kpoints_band(3,1))
      kpoints_band = 0.d0
      return
    endif

    open(11, file=trim(fn), form="formatted", status="old")
    read(11,*) nk_band

    allocate(kpoints_band(3,nk_band))

    do ik = 1, nk_band
      read(11,*) kpoints_band(:,ik)
    enddo
    close(11)

  end subroutine read_kpoints_band

end module kgrid
