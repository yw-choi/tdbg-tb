MODULE ELECTRONS_SOLVER_ELPA
  USE MPI
  USE CONSTANTS, ONLY: TPI
  USE KINDS, ONLY: DP
  USE DIAG, ONLY: DIAGH

  USE ENV, ONLY: &
    DATA_DIR

  USE LATTICE, ONLY: &
    LATT, RELATT, &
    NA_UC, ATOMS_UC, &
    NA_SC, ATOMS_SC, &
    SC_UC_MAP, NEIGHBORS, &
    NUM_NEIGHBORS, MAX_NEIGHBORS, &
    ISC_MAP, R_ISC

  USE IO, ONLY: &
    IO_2D_REAL_FMT

  USE ELPA

  USE ELECTRONS_HOPPINGS, ONLY: &
    NHOP, HOP, HOP_PAIRS

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: SOLVE_TB_ELPA

  !! SCALAPACK variables
  INTEGER :: &
    CONTEXT, NPROW, NPCOL, NB, &
    MYROW, MYCOL, INFO,        &
    DESCA( 50 ), DESCZ( 50 ),  &
    N, NUMR, NUMC
  INTEGER, EXTERNAL :: NUMROC

  !! Local matrices
  COMPLEX(DP), ALLOCATABLE :: A(:,:), Z(:,:)
  REAL(DP), ALLOCATABLE :: W(:)

CONTAINS

  ! PARALLEL OVER ORBITALS.
  ! SCALAPACK IS USED
  subroutine solve_tb_elpa (nk, kpoints, nbnd, lb, hb, &
      write_cnk, band, enk)

    INTEGER, INTENT(IN) :: NK, nbnd, LB, HB
    REAL(DP), INTENT(IN) :: KPOINTS(3,NK)
    LOGICAL, INTENT(IN) :: WRITE_CNK, BAND
    REAL(DP), INTENT(OUT) :: ENK(nbnd, NK)

    INTEGER :: IK

    CLASS(ELPA_T), POINTER :: ELPA
    INTEGER :: SUCCESS

    CHARACTER(LEN=100) :: TIMESTR, FN

    COMPLEX(DP), ALLOCATABLE :: WORK(:)

    N  = NA_UC

    call setup_scalapack

    allocate(work(nb)) ! workspace for pzlawrite

    IF (ELPA_INIT(20180525) /= ELPA_OK) THEN
      CALL ERRORE("SOLVE_TB_ELPA", "ELPA API version not supported", 1)
    ENDIF

    ELPA => ELPA_ALLOCATE()

    call ELPA%SET("na", N, success)
    call ELPA%SET("nev", HB, success)
    call ELPA%SET("local_nrows", NUMR, success)
    call ELPA%SET("local_ncols", NUMC, success)
    call ELPA%SET("nblk", NB, success)
    call ELPA%SET("mpi_comm_parent", comm, success)
    call ELPA%SET("process_row", MYROW, success)
    call ELPA%SET("process_col", MYCOL, success)

    success = ELPA%SETUP()

    call ELPA%SET("solver", elpa_solver_1stage, success)

    enk = 0.d0

    !! main loop
    do ik = 1, nk
      WRITE(TIMESTR, "(a,i5,a,i5)") "Computing kpt # ", ik, '/', nk
      call timestamp (TIMESTR)

      !! build hamiltonian
      call build_hmat(kpoints(1:3,ik), A)

      !! diagonalize
      call mpi_barrier(comm, mpierr) ! for correct timings only
      call start_clock('diag_elpa')
      call elpa%eigenvectors(A, W, Z, success)
      call stop_clock('diag_elpa')

      ENK(1:nbnd,IK) = W(LB:HB)

      IF (WRITE_CNK) THEN
        call start_clock('io_cnk')
        if (band) then
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk_band.', ik, '.dat'
        else
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk.', ik, '.dat'
        endif
        call WRITE_2D_CMPLX_PARALLEL ( fn, na_uc, lb, hb, numr, numc, z )
        call stop_clock('io_cnk')
      ENDIF

    enddo ! ik

    if (master) then
      print *, "Finished Diagonalization."
    endif

    call elpa_deallocate(elpa)
    call elpa_uninit()
    call deallocate_scalapack

  end subroutine solve_tb_elpa

  subroutine setup_scalapack
    implicit none

    integer :: info

    IF (N.GT.32*NPROCS) THEN
      nb = 32 ! default
    ELSE
      nb = (N-1)/NPROCS + 1
    ENDIF

    nprow = nint(sqrt(dble(nprocs)))

    do while (nprow.gt.0 .and. mod(nprocs, nprow).ne.0) 
      nprow = nprow - 1
    enddo

    if (nprow.eq.0) then
      call errore("scalapack", "nprow cannot be determined", 1)
    endif

    npcol = nprocs/nprow

    call blacs_get( -1, 0, context )
    call blacs_gridinit( context, 'r', nprow, npcol )
    call blacs_gridinfo( context, nprow, npcol, myrow, mycol )

    numr = numroc( n, nb, myrow, 0, nprow )
    numc = numroc( n, nb, mycol, 0, npcol )

    allocate(a(numr, numc))
    allocate(z(numr, numc))
    allocate(w(n))

    call descinit( desca, n, n, nb, nb, 0, 0, context, numr, info )
    if (info.ne.0) then
      print *, info
      call errore("blacs", "desca failed", info)
    endif
    call descinit( descz, n, n, nb, nb, 0, 0, context, numr, info )
    if (info.ne.0) then
      print *, info
      call errore("blacs", "descz failed", info)
    endif
    
  end subroutine setup_scalapack

  subroutine deallocate_scalapack
    call blacs_gridexit(context)
    deallocate(w, a, z)
  end subroutine deallocate_scalapack

  subroutine build_hmat(k, h_loc)
    real(dp), intent(in) :: k(3)

    complex(dp), intent(out) :: h_loc(numr, numc)

    integer :: ihop, ia_uc, ja_sc, ja_uc
    real(dp) :: arg, err, r(3)
    complex(dp) :: alpha

    call start_clock ('build_hmat')

    h_loc = 0.d0

    do ihop = 1, nhop
      ia_uc = hop_pairs(1,ihop)
      ja_sc = hop_pairs(2,ihop)
      ja_uc = sc_uc_map(ja_sc)
      r = r_isc(1:3, isc_map(ja_sc))
      arg = tpi*sum(k*r)
      alpha = 0.5d0*hop(ihop)*cmplx(cos(arg),sin(arg),kind=dp)
      call pzeladd ( h_loc, ia_uc, ja_uc, desca, alpha ) 
      call pzeladd ( h_loc, ja_uc, ia_uc, desca, dconjg(alpha) ) 
    enddo

    call stop_clock ('build_hmat')
  end subroutine build_hmat

  subroutine write_2d_cmplx_parallel ( fn, n1, l2, h2, LDA, LDB, LZ )
    integer, intent(in) :: n1, l2, h2, LDA, LDB
    complex(dp), intent(inout) :: LZ(LDA,LDB)
    character(len=*), intent(in) :: fn

    complex(dp), allocatable :: tmp(:), Z_ALL(:,:), LLZ(:,:)
    integer :: reclen, i2, n2
    logical :: exst

    integer :: LOCr, LOCc, ibcolumns, nbcolumns, ibrows, nbrows, &
               col_max, row_max, columns, rows, irow, icol, col_all, &
               node, istatus(MPI_STATUS_SIZE), ierr

    allocate(tmp(n1))
    inquire(iolength=reclen) tmp
    deallocate(tmp)

    if (master) then
      inquire(file=trim(fn), exist=exst)

      if (exst) then
        open(11, file=trim(fn), status="old")
        close(11, status="delete")
      endif

      open(11, file=trim(fn), form="unformatted", &
        status="unknown", access="direct", recl=reclen)
      allocate(z_all(n1, h2-l2+1))
    endif

    n2 = h2-l2+1

    !Begin master part
    IF (rank.EQ.0) THEN
      
      !obtain dimensions of the local array on master node
      LOCr = LDA
      LOCc = LDB

      ALLOCATE(LLZ(LOCr,LOCc))
      LLZ(1:LOCr,1:LOCc) = LZ(1:LOCr,1:LOCc)

      !k is the rank of the processor that sent the data
      !nprocs is the number of processors in the communicator
      DO node = 0, nprocs -1 
        IF (node.EQ.0) THEN
          irow = myrow
          icol = mycol
        ELSE

          !node -- is the rank of the processor that sent the data
          !status -- message status array(of type integer)
          CALL MPI_Recv(irow, 1, MPI_Integer, NODE, 10, MPI_COMM_WORLD, ISTATUS, IERR)
          CALL MPI_Recv(icol, 1, MPI_Integer, NODE, 20, MPI_COMM_WORLD, ISTATUS, IERR)

          CALL MPI_Recv(LOCr, 1, MPI_Integer, NODE, 40, MPI_COMM_WORLD, ISTATUS, IERR)
          CALL MPI_Recv(LOCc, 1, MPI_Integer, NODE, 50, MPI_COMM_WORLD, ISTATUS,IERR )

          !create a local matrix with the size of the matrix passed
          DEALLOCATE(LLZ)
          ALLOCATE( LLZ(locr,locc))

          !recieve the local matrix sent from node
          CALL MPI_Recv(LLZ, locr*locc, MPI_DOUBLE_COMPLEX, NODE, 30,MPI_COMM_WORLD, ISTATUS, IERR)
        ENDIF

        !compute the number of blocks in each local array
        nbrows = CEILING(REAL(LOCr)/REAL(NB))    !number of blocks in the row
        nbcolumns = CEILING(REAL(LOCc)/REAL(NB))    !number of blocks in the columns

        !loop over each block in the column
        DO ibcolumns = 1, nbcolumns  
          !special case - number of columns is less than NB     
          IF(ibcolumns.EQ.nbcolumns) THEN
            col_max = locc-(nbcolumns-1)*NB 
          ELSE
            col_max = NB !number of columns in a block        
          ENDIF

          !loop over each block in the row
          DO ibrows = 1, nbrows
            !special case - number of columns is less than NBCO
            IF (ibrows.EQ.nbrows) THEN
              row_max = locr-(nbrows-1)*NB
            ELSE
              row_max = NB !number of rows in a block
            ENDIF
            !for each column in the block, loop over each row in the block
            DO columns = 1, col_max
              DO rows = 1, row_max
                col_all = (icol+(ibcolumns-1)*npcol)*NB + columns
                IF (l2.le.col_all.and.col_all.le.h2) then

                  Z_ALL((irow+(ibrows-1)*nprow)*NB + rows, col_all-l2+1)&
                    = LLZ((ibrows-1)*NB + rows, (ibcolumns-1)*NB + columns)
                ENDIF
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !End master part.
    ELSE 
      ! Begin slave part.
      !send the processor coordinates in grid
      CALL MPI_Send(myrow, 1, MPI_Integer, 0, 10, MPI_COMM_WORLD, IERR)
      CALL MPI_Send(mycol, 1, MPI_Integer, 0, 20, MPI_COMM_WORLD, IERR)
      !send number rows and columns
      CALL MPI_Send(LDA, 1, MPI_Integer, 0, 40, MPI_COMM_WORLD, IERR)
      CALL MPI_Send(LDB, 1, MPI_Integer, 0, 50, MPI_COMM_WORLD, IERR)
      !send the local matrix
      CALL MPI_Send(LZ,LDA*LDB, MPI_DOUBLE_COMPLEX, 0, 30, MPI_COMM_WORLD, IERR)

    ENDIF
    ! End slave part.

    if (master) then
      do i2 = 1, h2-l2+1
        write(11, rec=i2) z_all(:,i2)
      enddo
      close(11)
      DEALLOCATE(Z_ALL, LLZ)
    endif

    call mpi_barrier(comm, mpierr)

  end subroutine write_2d_cmplx_parallel
end module electrons_solver_ELPA
