module electrons_hoppings
  use mpi
  use kinds, only: DP
  use constants, only: eps12, eps6, RYTOEV, ANGSTROM_AU
  use lattice, only: latt, relatt, na_uc, atoms_uc, na_sc, atoms_sc, &
                     sc_uc_map, neighbors, num_neighbors, max_neighbors, &
                     isc_map, R_isc, nlayer, na_uc_l, d_layer
                     

  implicit none
  
  ! Subroutines
  public :: calc_hoppings, dtdx

  ! Variables
  public :: &
    rcut, &
    efield, &
    nhop, &
    hop_pairs, &
    hop, &
    nnz, &
    csr_ia, &
    csr_ja  
  
  private

  real(dp) :: &
    rcut, &  ! hopping function cutoff
    efield   ! vertical efield (Ry/Bohr)

  integer :: &
    nhop, & ! number of hopping elements in real space
    nnz     ! number of nonzero matrix elements

  integer, allocatable :: &
    hop_pairs(:,:), & ! hop_pairs(2,nhop) (ia_uc, ja_sc) pairs
    csr_ia(:), &
    csr_ja(:)         

  real(DP), allocatable :: &
    hop(:) ! hop(nhop) hopping parameters

  ! Slater-Koster tight-binding parameters
  real(DP), parameter :: &
    Vpi  = -2.7d0/RYTOEV   , &
    Vsig =  0.48d0/RYTOEV  , &
    a0   =  2.46d0/sqrt(3.d0)*ANGSTROM_AU,  &
    d0   =  3.35d0*ANGSTROM_AU, &
    r0   =  0.184d0 * 2.46d0*ANGSTROM_AU

contains

  subroutine calc_hoppings 
    integer :: ia, ja, ihop, inb, ja_uc, il, iiz
    real(DP) :: at1(3), at2(3), dx(3), dist, dfac
    logical :: efield_on
    integer, allocatable :: nnz_flag(:)

    call start_clock('hoppings')
    call printblock_start ('Calculating hopping parameters')

    if (master) then
      call timestamp('calc_hoppings')
    endif

    !! Count number of hoppings for a given rcut
    nhop = 0
    nnz  = 0
    allocate(nnz_flag(na_uc))

    do ia = 1, na_uc
      at1 = atoms_uc(1:3, ia)

      do inb = 1, num_neighbors(ia)
        ja = neighbors(inb, ia)
        ! skip the same atom (dist.lt.eps6)
        dist = norm2(atoms_sc(1:3, ja) - at1)
        if (dist.gt.eps6.and.dist.lt.rcut) then
          nhop = nhop + 1
        endif
      enddo
    enddo

    efield_on = abs(efield).gt.eps12

    if (efield_on) then
      nhop = nhop + na_uc
    endif

    allocate(hop_pairs(2,nhop))
    allocate(hop(nhop))
    allocate(csr_ia(na_uc+1), csr_ja(nhop))
    hop_pairs = -1
    hop = 0.d0
    csr_ia(1) = 1
    iiz = 0

    !! Calculate hopping amplitudes
    ihop = 0
    do ia = 1, na_uc
      at1 = atoms_uc(1:3, ia)

      nnz_flag(:) = 0

      do inb = 1, num_neighbors(ia)
        ja = neighbors(inb, ia)
        ja_uc = sc_uc_map(ja)
        nnz_flag(ja_uc) = 1

        at2 = atoms_sc(1:3, ja) 
        dx = at1 - at2
        dist = norm2(dx)

        if (dist.lt.eps6.or.dist.gt.rcut) then
          cycle
        endif

        ihop = ihop + 1

        dfac = (dx(3)/dist)**2

        hop(ihop) = Vpi*exp(-(dist-a0)/r0)*(1.d0-dfac)+Vsig*exp(-(dist-d0)/r0)*dfac

        hop_pairs(1,ihop)   = ia
        hop_pairs(2,ihop)   = ja
      enddo

      !! efield
      if (efield_on) then
        il = int((ia-1)/na_uc_l)+1 
        ihop = ihop+1
        ! hop(ihop) = -efield*(il-1)*d_layer
        hop(ihop) = efield*atoms_uc(3,ia)
        hop_pairs(1,ihop) = ia
        hop_pairs(2,ihop) = ia
        nnz_flag(ia) = 1
      endif

      csr_ia(ia+1) = csr_ia(ia) + sum(nnz_flag)
      nnz = nnz + sum(nnz_flag)
      do ja_uc = 1, na_uc
        if (nnz_flag(ja_uc).eq.1) then
          iiz = iiz + 1
          csr_ja(iiz) = ja_uc
        endif
      enddo
    enddo

    if (ihop.ne.nhop) then
      print *, 'ihop, nhop = ', ihop, nhop
      call errore('electrons_hopping', 'insufficient hopping parameters', 1)
    endif

    if (master) then
      print *, "Number of hopping parameters = ", nhop
      print *, "Number of nonzero matrix elements = ", nnz
    endif

    call printblock_end
    call stop_clock('hoppings')

  end subroutine calc_hoppings

  ! derivative of hopping function w.r.t. ix axis,
  ! evaluated at d
  real(dp) function dtdx ( d, ix ) 
    integer, intent(in) :: ix
    real(dp), intent(in) :: d(3)
    real(dp) :: dist, fac1, fac2

    dist = norm2(d)

    !! Do not calculate duplicate parts
    if (ix.eq.1 .or. ix.eq.2) then

      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(ix)*d(3)*d(3)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) + fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 -fac2 ) 

    else
      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(3)*(dist**2-d(3)**2)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) - fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 + fac2 )
    endif

  end function dtdx
end module electrons_hoppings
