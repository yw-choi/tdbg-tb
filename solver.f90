module solver
  use lattice, only: na, N_LAYER
  use mpi
  use constants, only: AMU_RY, RYTOEV, PI, ANGSTROM_AU, RY_TO_CMM1
  use kinds, only: DP
  use fc, only: nfc_intra, idx_intra, d_intra, fc_intra, &
                nfc_inter, idx_inter, d_inter, fc_inter, &
                fc_diag, interlayer_on, mass,            &
                dynmat, csr_ia, csr_ja, nnz
  use diag, only: diagh, diag_feast
  implicit none

  public

contains

  subroutine solve_ph(iq, q, ibu, wn2, un)
    real(dp), intent(in) :: q(2)
    integer, intent(in) :: ibu, iq
    real(dp), intent(out) :: wn2(ibu)
    complex(dp), intent(out) :: un(3*N_LAYER*na,ibu)

    integer :: ia, ja, i1, i2, isc1, isc2, il, m1, m2, ifc, i, j
    integer :: cnt, nb
    real(dp) :: arg, s

    complex(dp), allocatable :: dyn(:,:)

    call start_clock ('solve_ph')

    allocate(dyn(3*N_LAYER*na, 3*N_LAYER*na))
    dyn = (0.d0, 0.d0)

    do ia = 1, N_LAYER*na
      do m1 = 1,3
        do m2 = 1,3
          dyn(3*(ia-1)+m1, 3*(ia-1)+m2) = dyn(3*(ia-1)+m1, 3*(ia-1)+m2)+fc_diag(m2,m1,ia) 
        enddo
      enddo
    enddo

    do ifc = 1, nfc_intra
      arg = q(1)*d_intra(1,ifc)+q(2)*d_intra(2,ifc)
      do m1 = 1,3
        do m2 = 1,3
          dyn(3*(idx_intra(1,ifc)-1) + m1, 3*(idx_intra(2,ifc)-1) + m2) = & 
              dyn(3*(idx_intra(1,ifc)-1) + m1, 3*(idx_intra(2,ifc)-1) + m2) &
              + fc_intra(m2,m1,ifc)*cmplx(cos(arg),sin(arg),kind=DP)
        enddo
      enddo 
    enddo

    if (interlayer_on) then
      do ifc = 1, nfc_inter
        arg = q(1)*d_inter(1,ifc)+q(2)*d_inter(2,ifc)
        do m1 = 1,3
          do m2 = 1,3
            dyn(3*(idx_inter(1,ifc)-1) + m1, 3*(idx_inter(2,ifc)-1) + m2) = & 
                dyn(3*(idx_inter(1,ifc)-1) + m1, 3*(idx_inter(2,ifc)-1) + m2) &
                + fc_inter(m2,m1,ifc)*cmplx(cos(arg),sin(arg),kind=DP)
          enddo
        enddo 
      enddo
    endif
    
    do i = 1, 3*N_LAYER*na
      dyn(i,i) = cmplx(dble(dyn(i,i)), 0.d0, kind=DP)
      do j = 1, i-1
        dyn(i,j) = 0.5d0*(dyn(i,j)+conjg(dyn(j,i)))
        dyn(j,i) = conjg(dyn(i,j))
      enddo
    enddo

    dyn = dyn  / (amu_ry*mass)

    nb = ibu
    call diagh(dyn, 3*N_LAYER*na, 1, ibu, nb, wn2, un)
    call stop_clock ('solve_ph')
  end subroutine solve_ph

  subroutine solve_ph2(iq, qvec, maxnev, wmin, wmax, wn2, un, nev)
    real(dp), intent(in) :: qvec(2), wmin, wmax
    integer, intent(in) :: maxnev, iq
    real(dp), intent(out) :: wn2(maxnev)
    complex(dp), intent(out) :: un(3*na*N_LAYER,maxnev)
    integer, intent(out) :: nev

    integer :: n

    real(dp) :: w2min, w2max


    call start_clock ('solve_ph2')

    n = 3*na*N_LAYER

    call start_clock ('build_dynmat')

    call build_dynmat_csr ( qvec )

    call stop_clock ('build_dynmat')
    if (wmin.lt.0.d0) then
      w2min = -wmin**2
    else
      w2min = wmin**2
    endif
    w2max = wmax**2

    call start_clock ('diag_feast')
    call diag_feast ( n, nnz, dynmat, csr_ia, csr_ja, w2min, w2max, maxnev, wn2, un, nev )
    call stop_clock ('diag_feast')

    call mpi_barrier(comm, mpierr)

    call stop_clock ('solve_ph2')
  end subroutine solve_ph2

  subroutine build_dynmat_csr ( qvec )
    real(dp), intent(in) :: qvec(2)

    integer :: ifc, ia, i, j, k, idx, m1, m2
    integer :: prev_index, nnz_idx
    complex(dp) :: cfac
    real(dp) :: arg
    
    integer :: cnt

    dynmat = 0.d0

    do ifc = 1, nfc_intra
      arg = qvec(1)*d_intra(1,ifc)+qvec(2)*d_intra(2,ifc)
      cfac = cmplx(cos(arg),sin(arg),kind=dp)
      do m1 = 1,3
        do m2 = 1,3
          i = 3*(idx_intra(1,ifc)-1) + m1 
          j = 3*(idx_intra(2,ifc)-1) + m2 

          k = find_index(csr_ia(i+1)-csr_ia(i), csr_ja(csr_ia(i):csr_ia(i+1)), j)

          if (k.eq.-1) then
            print *, 'ifc_intra = ', ifc
            print *, 'i,j = ', i, j
            print *, 'csr_ja dump'
            do k = 1,csr_ia(i+1)-csr_ia(i)
              print *, csr_ja(csr_ia(i)+k)
            enddo
            call errore('build_dynmat_csr', 'Cannot find column index', 1)
          endif

          dynmat(csr_ia(i)+k-1) = dynmat(csr_ia(i)+k-1)+fc_intra(m2,m1,ifc)*cfac

        enddo
      enddo
    enddo

    do ifc = 1, nfc_inter
      arg = qvec(1)*d_intra(1,ifc)+qvec(2)*d_intra(2,ifc)
      cfac = cmplx(cos(arg),sin(arg),kind=dp)
      do m1 = 1,3
        do m2 = 1,3
          i = 3*(idx_inter(1,ifc)-1) + m1 
          j = 3*(idx_inter(2,ifc)-1) + m2 

          k = find_index(csr_ia(i+1)-csr_ia(i), csr_ja(csr_ia(i):csr_ia(i+1)), j)

          if (k.eq.-1) then
            print *, 'ifc_inter = ', ifc
            print *, 'i,j = ', i, j
            call errore('build_dynmat_csr', 'Cannot find column index', 1)
          endif

          dynmat(csr_ia(i)+k-1) = dynmat(csr_ia(i)+k-1)+fc_inter(m2,m1,ifc)*cfac
        enddo
      enddo
    enddo

    do ia = 1, na*N_LAYER
      do m1 = 1,3
        do m2 = 1,3
          i = 3*(ia-1) + m1 
          j = 3*(ia-1) + m2 

          k = find_index(csr_ia(i+1)-csr_ia(i), csr_ja(csr_ia(i):csr_ia(i+1)), j)

          if (k.eq.-1) then
            print *, 'ia = ', ia
            print *, 'i,j = ', i, j
            call errore('build_dynmat_csr', 'Cannot find column index', 1)
          endif

          dynmat(csr_ia(i)+k-1) = dynmat(csr_ia(i)+k-1)+fc_diag(m2,m1,ia)
        enddo
      enddo
    enddo

    dynmat = dynmat / (amu_ry*mass)

  end subroutine build_dynmat_csr 

  integer function find_index(n, ar, val) result(idx)
    integer :: n, ar(n), val

    integer :: i 

    idx = -1
    do i = 1, n
      if (ar(i).eq.val) then
        idx = i
        return
      endif
    enddo
    return
  end function find_index
end module solver
