module diag
  use kinds, only: DP

  private

  public :: diagh, diagh_full, diag_feast

contains

  subroutine diagh(N, H, il, iu, ek, uk)
    integer, intent(in) :: N, il, iu
    complex(dp), intent(inout) :: H(N, N)
    real(dp), intent(out) :: ek(iu-il+1)
    complex(dp), intent(out) :: uk(N,iu-il+1)

    ! local variables
    integer :: nh, info, lwork, lrwork, liwork, &
               isuppz(2*n), dummy3(1), nb
    complex(dp) :: dummy1(1)
    real(dp) :: dummy2(1)
    integer, allocatable :: iwork(:)
    complex(dp), allocatable :: work(:)
    real(dp), allocatable :: rwork(:)

    real(dp), allocatable :: w(:)

    nb = iu - il + 1

    allocate(w(n))
    call ZHEEVR('V', 'I', 'U', N, H, N, 0.d0, 0.d0, &
                il, iu, -1.0, nb, &
                w, uk, N, isuppz, dummy1, -1, dummy2, -1, &
                dummy3, -1, info)
    lwork = dummy1(1)
    lrwork = dummy2(1)
    liwork = dummy3(1)
    allocate(work(lwork))
    allocate(rwork(lrwork))
    allocate(iwork(liwork))
    call ZHEEVR('V', 'I', 'U', N, H, N, 0.0d0, 0.d0, &
                il, iu, -1.0, nb, &
                w, uk, N, isuppz, work, lwork, rwork, lrwork, &
                iwork, liwork, info)

    ek(1:nb) = w(1:nb)

    if (info.ne.0) then
        write(*,*) "ZHEEVR info = ", info
        return
    endif

    deallocate(w)
    deallocate(work)
    deallocate(rwork)
    deallocate(iwork)
  end subroutine diagh

  subroutine diagh_full ( N, A, W )
    integer, intent(in) :: N
    complex(dp), intent(inout) :: A(N, N)
    real(dp), intent(out) :: W(N)

    integer :: INFO, LWORK

    complex(dp), allocatable :: WORK(:)
    complex(dp) :: LWORK_OPT
    real(dp) :: RWORK(3*N-2)

    LWORK = -1
    call ZHEEV ( 'V', 'U', N, A, N, W, LWORK_OPT, LWORK, RWORK, INFO )

    LWORK = INT(LWORK_OPT)
    allocate(WORK(LWORK))

    call ZHEEV ( 'V', 'U', N, A, N, W, WORK, LWORK, RWORK, INFO )

    if (INFO.ne.0) then
      call errore('diagh_full', 'diagonalization failed', INFO)
    endif

  end subroutine diagh_full

  subroutine diag_feast (pool_comm, N, NNZ, A, IA, JA, evmin, evmax, M0, eigval, eigvec, M) 
    integer, intent(inout) :: N, NNZ, M0, IA(N+1), JA(NNZ)
    real(dp), intent(inout) :: evmin, evmax
    integer, intent(inout) :: pool_comm
    complex(dp), intent(inout) :: A(NNZ)
    real(dp), intent(out) :: eigval(M0)
    complex(dp), intent(out) :: eigvec(N,M0)
    integer, intent(out) :: M

    character(len=1) :: UPLO 
    integer :: fpm(64)
    real(dp) :: epsout
    integer :: loop, info, i
    real(dp), allocatable :: resid(:)

    allocate(resid(M0))
    call feastinit(fpm)
    UPLO = 'F'
    fpm(1) = 0
    fpm(9) = pool_comm 

    call zfeast_hcsrev(UPLO,N,A,IA,JA,fpm,epsout,loop,&
      evmin,evmax,M0,eigval,eigvec,M,resid,info)

    if (info.ne.0) then
      if (info.eq.3) then
        print *, "feast_M0 is too small. Increase feast_M0 ", M0
      endif
      call errore('diag_feast', 'FEAST failed', info)
    endif

    ! if (rank==0) then
    !   print *,'FEAST OUTPUT INFO',info
    !   if (info==0) then
    !      print *,'*************************************************'
    !      print *,'************** REPORT ***************************'
    !      print *,'*************************************************'
    !      print *,'# processors',nprocs
    !      print *,'# Search interval [evmin,evmax]',evmin,evmax
    !      print *,'# mode found/subspace',M,M0
    !      print *,'# iterations',loop
    !      print *,'TRACE',sum(eigval(1:M))
    !      print *,'Relative error on the Trace',epsout
    !      print *,'Eigenvalues/Residuals'
    !      do i=1,M
    !         print *,i,eigval(i),resid(i)
    !      enddo
    !   endif
    ! endif
  end subroutine diag_feast
end module diag
