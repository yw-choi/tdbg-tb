program tb

  use mpi
  use env, only: &
    data_dir

  use input, only: &
    read_input

  use lattice, only: &
    setup_lattice

  use kgrid, only: &
    nk, nkb, &
    setup_kgrid

  use electrons, only: &
    nbnd, &
    read_enk, &
    load_enk, &
    setup_band_indices, &
    solve_tb, &
    solve_tb_band

  use electrons_hoppings, only: &
    rcut, &
    calc_hoppings 

  use dos, only: &
    setup_egrid, &
    dos_ne, &
    calc_tdos, &
    l_layer_pdos, &
    calc_layer_pdos, &
    l_layer_weights_band, &
    calc_layer_weights_band

  use berry, only: &
    lberry, &
    calc_berry_phase

  implicit none

  integer :: ja_sc

  call mpi_initialize
  call timestamp('Start of run')

  call init_clocks ( .true. )
  call start_clock ( 'tb' ) 

  if (master) then
    print '(a)', repeat('=',60)
    print '(a,a,a)', '== ','Tight-binding calculation for twisted bilayer-bilayer graphene',' =='
    print '(a)', repeat('=',60)

    print *, 'Running on ', nprocs, ' nodes'
    print '(a)', repeat('=',60)
  endif

  !! SETUP
  call read_input
  
  call setup_lattice ( rcut )

  call setup_band_indices
  call setup_kgrid
  call setup_egrid

  !! Calculate hopping parameters if needed.
  if (.not.read_enk .or. nkb.gt.0) then
    call calc_hoppings
  endif

  !! Solve TB or read enk for uniform k-grid
  if (read_enk) then
    call load_enk
  else
    call solve_tb
  endif

  !! DOS
  if (dos_ne.gt.0) then
    call calc_tdos
    if (l_layer_pdos) then
      call calc_layer_pdos
    endif
  endif

  !! BAND
  if (nkb.gt.0) then
    call solve_tb_band
  endif

  if (nkb.gt.0 .and. l_layer_weights_band) then
    call calc_layer_weights_band
  endif

  if (lberry) then
    call calc_berry_phase
  endif

  call stop_tb
end program tb
